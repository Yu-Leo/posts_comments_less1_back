package mock

var Repo = Repository{lastPostID: 4, lastCommentID: 4}

type Repository struct {
	lastPostID    int
	lastCommentID int
}
